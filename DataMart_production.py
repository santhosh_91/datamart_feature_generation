
# coding: utf-8

# In[1]:


import os 
import sys
from dateutil import relativedelta
import datetime
from sqlalchemy import create_engine
from boto.s3.connection import S3Connection
from pyspark.sql.types import *
from pyspark.sql import functions as F
from pyspark.sql.window import Window
import calendar
import logging


# Setting environment and variables to be used

# In[2]:


logger = logging.getLogger(__name__)
hdlr = logging.FileHandler('DataMart_' + str(datetime.datetime.now()) + '_py.log')
formatter = logging.Formatter('%(asctime)s %(module)s %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


# In[3]:


logger.info("Setting Environment and Variables")


# In[4]:


src_host='nonprod-analytics-redshit01.cvdlsqkayfsg.ap-southeast-1.redshift.amazonaws.com'
src_port='5439'
src_db='redshift01'
src_user='lac'
src_pwd='Link1234'


# In[5]:


datatype_dict={"string":StringType(),"integer":IntegerType(),"float":FloatType()}


# In[6]:


try:
    __IPYTHON__
    corporationID='104940000000'
    startMonth=10
    startYear=2017
    endMonth=3
    endYear=2018
    schema='link'
    table='view_facttrans_plus'
    file_name='Caltex'
    folder_name='Caltex'

    
except NameError:
    corporationID=sys.argv[1]
    startMonth=int(sys.argv[2])
    startYear=int(sys.argv[3])
    endMonth=int(sys.argv[4])
    endYear=int(sys.argv[5])
    schema=sys.argv[6]
    table=sys.argv[7]
    file_name=sys.argv[8]
    folder_name=sys.argv[9]


# In[7]:


columnDataType=[['csn','string'],
['corporation_id','string'],
['branch_no','string'],
['transaction_date','string'],
['monthid','integer'],
['yearid','integer'],
['scheme_id','string'],
['pool_id','string'],
['pointid','string'],
['product_account_type','integer'],
['point_earned','float'],
['est_gross_transaction_amt','float'],
['transcount','integer']]


# In[8]:


columns=[field_name for field_name, dtype in columnDataType]


# In[9]:


startYearTransactions=(datetime.date(startYear, startMonth, 1)-  relativedelta.relativedelta(months=6)).year


# In[10]:


startMonthTransaction=(datetime.date(startYear, startMonth, 1)-  relativedelta.relativedelta(months=6)).month


# In[11]:


logger.debug("corporationID : %s startMonth : %s startYear : %s endMonth : %s endYear : %s schema : %s table : %s file_name : %s folder_name : %s startYearTransactions : %s startMonthTransaction : %s"            ,corporationID,startMonth,startYear,endMonth,endYear,schema,table,file_name,folder_name,startYearTransactions,startMonthTransaction)


# In[12]:


unload_stmt = """unload ('%s') to '%s' access_key_id '%s' secret_access_key '%s' allowoverwrite delimiter ',' addquotes"""


# In[13]:


conn = S3Connection('AKIAJ3BMMACHUPUSVIDQ','ymigRywiz1uiB+3CGfJVLQBi05NlB2K108CWoSZe')


# In[14]:


engine = create_engine('postgresql://'+src_user+':'+src_pwd+'@'+src_host+':'+src_port+'/'+src_db)


# In[27]:


query_stmt="""select %s from %s.%s a join ( select distinct csn as csn1 from %s.%s where corporation_id=''%s'' and transaction_date between TO_DATE(''01 %s %s'',''DD MM YYYY'') and last_day(TO_DATE(''01 %s %s'',''DD MM YYYY''))) b on a.csn=b.csn1  where a.transaction_date between TO_DATE(''01 %s %s'',''DD MM YYYY'') and last_day(TO_DATE(''01 %s %s'',''DD MM YYYY'')) and lower(pool_id)=''p01'' and lower(club_code)=''lnk''"""


# In[28]:


#For development
#query_stmt="""select %s from %s.%s a join ( select top 5000 distinct csn as csn1 from %s.%s where corporation_id=''%s'' and transaction_date between TO_DATE(''01 %s %s'',''DD MM YYYY'') and last_day(TO_DATE(''01 %s %s'',''DD MM YYYY''))) b on a.csn=b.csn1  where a.transaction_date between TO_DATE(''01 %s %s'',''DD MM YYYY'') and last_day(TO_DATE(''01 %s %s'',''DD MM YYYY'')) and lower(pool_id)=''p01'' and lower(club_code)=''lnk''"""


# In[29]:


query=query_stmt%(",".join(columns),schema,table,schema,table,corporationID,startMonth,startYear,endMonth,endYear,startMonthTransaction,startYearTransactions,endMonth,endYear)


# In[31]:


print(query)


# In[18]:


logger.debug("query : %s",query)


# In[19]:


logger.info("Initilaising Spark Environment")


# In[20]:


os.environ['PYSPARK_SUBMIT_ARGS'] = '--conf spark.executor.extraClassPath=/home/lac-user/Chaoran/RedshiftJDBC4-1.2.1.1001.jar                                      --driver-class-path /home/lac-user/Chaoran/RedshiftJDBC4-1.2.1.1001.jar                                      --jars /home/lac-user/Chaoran/RedshiftJDBC4-1.2.1.1001.jar                                      --packages com.amazonaws:aws-java-sdk:1.10.34,org.apache.hadoop:hadoop-aws:2.6.0                                      --master local[*] pyspark-shell'
from pyspark.sql import SparkSession
spark = (SparkSession
    .builder
    .appName("datamart")
    .config("spark.driver.memory","10g")
    .config("spark.executor.memory","10g")
    #.config("spark.some.config.option", "some-value")\
    .getOrCreate())

hadoopConf = spark._jsc.hadoopConfiguration()
hadoopConf.set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
hadoopConf.set("fs.s3.awsAccessKeyId", 'AKIAJ3BMMACHUPUSVIDQ')
hadoopConf.set("fs.s3.awsSecretAccessKey", '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')


# In[21]:


logger.info("Spark Environment Initialized")
logger.info("Executing Unload statement on Redshift")


# In[173]:


engine.execute(unload_stmt%(query,'s3://link-lac-data/Data Mart/'+folder_name+'/'+file_name,'AKIAJ3BMMACHUPUSVIDQ','ymigRywiz1uiB+3CGfJVLQBi05NlB2K108CWoSZe'))


# In[174]:


#unload_stmt%(query,'s3://link-lac-data/Data Mart/'+folder_name+'/'+file_name,'AKIAJ3BMMACHUPUSVIDQ','ymigRywiz1uiB+3CGfJVLQBi05NlB2K108CWoSZe')


# In[83]:


logger.info("Unload statement Executed")


# # Reading from S3

# In[175]:


fields=[StructField(field_name.replace("est_gross_transaction_amt","gross_transaction_amt"), datatype_dict.get(dtype), True) for field_name,dtype in columnDataType]


# In[176]:


logger.debug("Fields : %s",fields)


# In[177]:


logger.info("Reading csv from S3")


# In[178]:


logger.debug("Load from Location : %s","s3://link-lac-data/Data Mart/"+folder_name+"/*")


# In[261]:


df=spark.read.format("csv").option("header", "false").load("s3://link-lac-data/Data Mart/"+folder_name+"/*",schema=StructType(fields))


# In[262]:


logger.info("csv from S3 read successfully")


# # Transformations

# In[263]:


df_order=df.select('yearid','monthid').distinct().sort('yearid','monthid')


# In[264]:


w = Window().orderBy('yearid','monthid')
df_order= df_order.withColumn("order_no", F.row_number().over(w))


# In[265]:


df=df.join(df_order,['yearid','monthid'])


# Merchant Monthly Issuance Aggregation

# In[266]:


logger.info("Merchant Monthly Issuance Aggregation")


# In[268]:


df_merchant_issuance=df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))==corporationID.lower())                               &(F.col("transaction_date")>=datetime.date(startYear, startMonth, 1))&                              (F.col("transaction_date")<=datetime.date(endYear, endMonth, calendar.monthrange(endYear,endMonth)[1]))
                              ).groupby("csn","yearid","monthid","order_no").\
agg(F.countDistinct(F.col("branch_no")).alias("number_of_branches_m0"),\
                                                        F.sum(F.col("point_earned")).alias("points_earned_m0"),\
                                                        F.sum(F.col("gross_transaction_amt")).alias("sales_m0"),\
                                                        F.sum(F.col("transcount")).alias("transactions_m0"),\
                                                        (F.sum(F.col("gross_transaction_amt"))/ F.sum(F.col("transcount"))).alias("sales_per_transaction_m0"),\
                                                        F.countDistinct(F.col("product_account_type")).alias("number_of_cards_m0")\
                                                       )


# In[270]:


logger.info("Merchant Monthly Issuance Aggregation Whole Period")


# In[271]:


df_merchant_issuance_whole_period=df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))==corporationID.lower())                              # &(F.col("transaction_date")>=datetime.date(startYear, startMonth, 01))&\
                              #(F.col("transaction_date")<=datetime.date(endYear, endMonth, 01))
                              ).groupby("csn","yearid","monthid","order_no").\
agg(F.countDistinct(F.col("branch_no")).alias("number_of_branches_m0"),\
                                                        F.sum(F.col("point_earned")).alias("points_earned_m0"),\
                                                        F.sum(F.col("gross_transaction_amt")).alias("sales_m0"),\
                                                        F.sum(F.col("transcount")).alias("transactions_m0"),\
                                                        (F.sum(F.col("gross_transaction_amt"))/ F.sum(F.col("transcount"))).alias("sales_per_transaction_m0"),\
                                                        F.countDistinct(F.col("product_account_type")).alias("number_of_cards_m0")\
                                                       )


# Selected csn yearid monthid

# In[272]:


logger.info("Dataframe with only selected csn")


# In[273]:


df_csn_selected=df_merchant_issuance.select("csn","order_no").distinct()


# Latest 3 months Merchant Issuance Aggregation

# In[274]:


logger.info("Latest 3 months Merchant Issuance Aggregation")


# In[275]:


df_merchant_issuance_0_2months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))==corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=2) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_branches_m0_2"),                                                        F.sum(F.col("b.point_earned")).alias("points_earned_m0_2"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_m0_2"),                                                        F.sum(F.col("b.transcount")).alias("transactions_m0_2"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_transaction_m0_2"),                                                        F.countDistinct(F.col("b.product_account_type")).alias("number_of_cards_m0_2"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_transacted_m0_2")
                                                       )


# Previous 3 months Merchant Issuance Aggregation

# In[276]:


logger.info("Previous 3 months Merchant Issuance Aggregation")


# In[277]:


df_merchant_issuance_3_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))==corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=3)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_branches_m3_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_earned_m3_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_m3_5"),                                                        F.sum(F.col("b.transcount")).alias("transactions_m3_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_transaction_m3_5"),                                                        F.countDistinct(F.col("b.product_account_type")).alias("number_of_cards_m3_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_transacted_m3_5")
                                                       )


# Last 6 months Merchant Issuance Aggregation

# In[278]:


logger.info("Last 3 months Merchant Issuance Aggregation")


# In[279]:


df_merchant_issuance_0_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))==corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_branches_m0_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_earned_m0_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_m0_5"),                                                        F.sum(F.col("b.transcount")).alias("transactions_m0_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_transaction_m0_5"),                                                        F.countDistinct(F.col("b.product_account_type")).alias("number_of_cards_m0_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_transacted_m0_5")
                                                       )


# Merchant Monthly Redemption Aggregation

# In[280]:


logger.info("Merchant Monthly Redemption Aggregation")


# In[281]:


df_merchant_redemption=df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))==corporationID.lower())                              # &(F.col("transaction_date")>=datetime.date(startYear, startMonth, 01))&\
                              #(F.col("transaction_date")<=datetime.date(endYear, endMonth, calendar.monthrange(endYear,endMonth)[1]))
                              ).groupby("csn","order_no").\
agg(F.countDistinct(F.col("branch_no")).alias("number_of_redemption_branches_m0"),\
                                                        F.sum(F.col("point_earned")).alias("points_redeemed_m0"),\
                                                        F.sum(F.col("gross_transaction_amt")).alias("sales_redemption_m0"),\
                                                        F.sum(F.col("transcount")).alias("redemption_transactions_m0"),\
                                                        (F.sum(F.col("gross_transaction_amt"))/ F.sum(F.col("transcount"))).alias("sales_per_redemption_transaction_m0")\
   )


# Latest 3 months Merchant Redemption Aggregation

# In[282]:


logger.info("Latest 3 months Merchant Redemption Aggregation")


# In[283]:


df_merchant_redemption_0_2months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))==corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=2) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_redemption_branches_m0_2"),                                                        F.sum(F.col("b.point_earned")).alias("points_redeemed_m0_2"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_redemption_m0_2"),                                                        F.sum(F.col("b.transcount")).alias("redemption_transactions_m0_2"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_redemption_transaction_m0_2"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_redeemed_m0_2")
                                                       )


# Previous 3 months Merchant Redemption Aggregation

# In[284]:


logger.info("Previous 3 months Merchant Redemption Aggregation")


# In[285]:


df_merchant_redemption_3_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))==corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=3)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_redemption_branches_m3_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_redeemed_m3_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_redemption_m3_5"),                                                        F.sum(F.col("b.transcount")).alias("redemption_transactions_m3_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_redemption_transaction_m3_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_redeemed_m3_5")
                                                       )


# Last 6 months Merchant Redemption Aggregation

# In[286]:


logger.info("Last 6 months Merchant Redemption Aggregation")


# In[287]:


df_merchant_redemption_0_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))==corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_redemption_branches_m0_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_redeemed_m0_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_redemption_m0_5"),                                                        F.sum(F.col("b.transcount")).alias("redemption_transactions_m0_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_redemption_transaction_m0_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_redeemed_m0_5")
                                                       )


# Other Merchants Monthly Issuance Aggregation

# In[288]:


logger.info("Other Merchants Monthly Issuance Aggregation")


# In[289]:


df_other_merchants_issuance=df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))!=corporationID.lower())                             ##  &(F.col("transaction_date")>=datetime.date(startYear, startMonth, 01))&\
                              ##(F.col("transaction_date")<=datetime.date(endYear, endMonth, calendar.monthrange(endYear,endMonth)[1]))
                              ).groupby("csn","order_no").\
agg(F.countDistinct(F.col("branch_no")).alias("number_of_branches_other_merchants_m0"),\
                                                        F.sum(F.col("point_earned")).alias("points_earned_other_merchants_m0"),\
                                                        F.sum(F.col("gross_transaction_amt")).alias("sales_other_merchants_m0"),\
                                                        F.sum(F.col("transcount")).alias("transactions_other_merchants_m0"),\
                                                        (F.sum(F.col("gross_transaction_amt"))/ F.sum(F.col("transcount"))).alias("sales_per_transaction_other_merchants_m0"),\
                                                        F.countDistinct(F.col("product_account_type")).alias("number_of_cards_other_merchants_m0"),\
                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_other_merchants_m0")
                                                       )


# Latest 3 months Other Merchants Issuance Aggregation

# In[290]:


logger.info("Latest 3 months Other Merchants Issuance Aggregation")


# In[291]:


df_other_merchants_issuance_0_2months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))!=corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=2) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_branches_other_merchants_m0_2"),                                                        F.sum(F.col("b.point_earned")).alias("points_earned_other_merchants_m0_2"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_other_merchants_m0_2"),                                                        F.sum(F.col("b.transcount")).alias("transactions_other_merchants_m0_2"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_transaction_other_merchants_m0_2"),                                                        F.countDistinct(F.col("b.product_account_type")).alias("number_of_cards_other_merchants_m0_2"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_transacted_other_merchants_m0_2"),                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_other_merchants_m0_2")
                                                       )


# Previous 3 months Other Merchants Issuance Aggregation

# In[292]:


logger.info("Previous 3 months Other Merchants Issuance Aggregation")


# In[293]:


df_other_merchants_issuance_3_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))!=corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=3)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_branches_other_merchants_m3_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_earned_other_merchants_m3_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_other_merchants_m3_5"),                                                        F.sum(F.col("b.transcount")).alias("transactions_other_merchants_m3_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_transaction_other_merchants_m3_5"),                                                        F.countDistinct(F.col("b.product_account_type")).alias("number_of_cards_other_merchants_m3_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_transacted_other_merchants_m3_5"),                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_other_merchants_m3_5")
                                                       )


# Last 6 months Other Merchants Issuance Aggregation

# In[294]:


logger.info("Last 6 months Other Merchants Issuance Aggregation")


# In[295]:


df_other_merchants_issuance_0_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='i') & (F.lower(F.col("corporation_id"))!=corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_branches_other_merchants_m0_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_earned_other_merchants_m0_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_other_merchants_m0_5"),                                                        F.sum(F.col("b.transcount")).alias("transactions_other_merchants_m0_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_transaction_other_merchants_m0_5"),                                                        F.countDistinct(F.col("b.product_account_type")).alias("number_of_cards_other_merchants_m0_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_transacted_other_merchants_m0_5"),                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_other_merchants_m0_5")
                                                       )


# Other Merchants Monthly Redemption Aggregation

# In[296]:


logger.info("Other Merchants Monthly Redemption Aggregation")


# In[297]:


df_other_merchants_redemption=df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))!=corporationID.lower())                              # &(F.col("transaction_date")>=datetime.date(startYear, startMonth, 01))&\
                              #(F.col("transaction_date")<=datetime.date(endYear, endMonth, calendar.monthrange(endYear,endMonth)[1]))
                              ).groupby("csn","order_no").\
agg(F.countDistinct(F.col("branch_no")).alias("number_of_redemption_branches_other_merchants_m0"),\
                                                        F.sum(F.col("point_earned")).alias("points_redeemed_other_merchants_m0"),\
                                                        F.sum(F.col("gross_transaction_amt")).alias("sales_redemption_other_merchants_m0"),\
                                                        F.sum(F.col("transcount")).alias("redemption_transactions_other_merchants_m0"),\
                                                        (F.sum(F.col("gross_transaction_amt"))/ F.sum(F.col("transcount"))).alias("sales_per_redemption_transaction_other_merchants_m0"),\
                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_redeemed_other_merchants_m0")
   )


# Latest 3 months Other Merchants Redemption Aggregation

# In[298]:


logger.info("Latest 3 Months Other Merchants Redemption Aggregation")


# In[299]:


df_other_merchants_redemption_0_2months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))!=corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=2) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_redemption_branches_other_merchants_m0_2"),                                                        F.sum(F.col("b.point_earned")).alias("points_redeemed_other_merchants_m0_2"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_redemption_other_merchants_m0_2"),                                                        F.sum(F.col("b.transcount")).alias("redemption_transactions_other_merchants_m0_2"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_redemption_transaction_other_merchants_m0_2"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_redeemed_other_merchants_m0_2"),                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_redeemed_other_merchants_m0_2")
                                                       )


# Previous 3 months Other Merchants Redemption Aggregation

# In[300]:


logger.info("Previous 3 Months Other Merchants Redemption Aggregation")


# In[301]:


df_other_merchants_redemption_3_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))!=corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=3)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_redemption_branches_other_merchants_m3_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_redeemed_other_merchants_m3_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_redemption_other_merchants_m3_5"),                                                        F.sum(F.col("b.transcount")).alias("redemption_transactions_other_merchants_m3_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_redemption_transaction_other_merchants_m3_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_redeemed_other_merchants_m3_5"),                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_redeemed_other_merchants_m3_5")
    )


# Last 6 months Other Merchants Redemption Aggregation

# In[302]:


logger.info("Last 6 Months Other Merchants Redemption Aggregation")


# In[303]:


df_other_merchants_redemption_0_5months=df_csn_selected.alias('a').join(df.filter((F.lower(F.col("pointid"))=='r') & (F.lower(F.col("corporation_id"))!=corporationID.lower())).alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')<=5) &                                                             (F.col('a.order_no')-F.col('b.order_no')>=0)).drop(F.col("b.csn")).drop(F.col("b.order_no")).groupby("csn","order_no").agg(F.countDistinct(F.col("b.branch_no")).alias("number_of_redemption_branches_other_merchants_m0_5"),                                                        F.sum(F.col("b.point_earned")).alias("points_redeemed_other_merchants_m0_5"),                                                        F.sum(F.col("b.gross_transaction_amt")).alias("sales_redemption_other_merchants_m0_5"),                                                        F.sum(F.col("b.transcount")).alias("redemption_transactions_other_merchants_m0_5"),                                                        (F.sum(F.col("b.gross_transaction_amt"))/ F.sum(F.col("b.transcount"))).alias("sales_per_redemption_transaction_other_merchants_m0_5"),                                                        F.countDistinct(F.col("b.monthid")).alias("number_of_months_redeemed_other_merchants_m0_5"),                                                        F.countDistinct(F.col("corporation_id")).alias("number_of_merchants_redeemed_other_merchants_m0_5")
    )


# Final Join

# In[304]:


logger.info("Final Merchant Issuance Join")


# In[338]:


df_final_merchant_issuance=df_merchant_issuance.alias('a').join(df_merchant_issuance_whole_period.alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')==1),'left').join(df_merchant_issuance_whole_period.alias('c'),(F.col('a.csn')==F.col('c.csn')) &                                                              (F.col('a.order_no')-F.col('c.order_no')==2),'left').join(df_merchant_issuance_whole_period.alias('d'),(F.col('a.csn')==F.col('d.csn')) &                                                              (F.col('a.order_no')-F.col('d.order_no')==3),'left').join(df_merchant_issuance_whole_period.alias('e'),(F.col('a.csn')==F.col('e.csn')) &                                                              (F.col('a.order_no')-F.col('e.order_no')==4),'left').join(df_merchant_issuance_whole_period.alias('f'),(F.col('a.csn')==F.col('f.csn')) &                                                              (F.col('a.order_no')-F.col('f.order_no')==5),'left').join(df_merchant_issuance_0_2months.alias('g'),(F.col('a.csn')==F.col('g.csn')) &                                                              (F.col('a.order_no')==F.col('g.order_no')),'left').join(df_merchant_issuance_3_5months.alias('h'),(F.col('a.csn')==F.col('h.csn')) &                                                              (F.col('a.order_no')==F.col('h.order_no')),'left').join(df_merchant_issuance_0_5months.alias('i'),(F.col('a.csn')==F.col('i.csn')) &                                                              (F.col('a.order_no')==F.col('i.order_no')),'left').select('a.*'        ,F.col('b.points_earned_m0').alias('points_earned_m1')        ,F.col('c.points_earned_m0').alias('points_earned_m2')        ,F.col('d.points_earned_m0').alias('points_earned_m3')        ,F.col('e.points_earned_m0').alias('points_earned_m4')        ,F.col('f.points_earned_m0').alias('points_earned_m5')        ,F.col('g.points_earned_m0_2')        ,F.col('h.points_earned_m3_5')        ,F.col('i.points_earned_m0_5')        ,(F.col('b.points_earned_m0')/F.col('a.points_earned_m0')).alias('points_earned_ratio_m0_1')        ,(F.col('c.points_earned_m0')/F.col('a.points_earned_m0')).alias('points_earned_ratio_m0_2')        ,(F.col('d.points_earned_m0')/F.col('a.points_earned_m0')).alias('points_earned_ratio_m0_3')        ,(F.col('e.points_earned_m0')/F.col('a.points_earned_m0')).alias('points_earned_ratio_m0_4')        ,(F.col('f.points_earned_m0')/F.col('a.points_earned_m0')).alias('points_earned_ratio_m0_5')        ,(F.col('a.points_earned_m0')-F.when(F.col('b.points_earned_m0').isNull(),F.lit(0)).otherwise(F.col('b.points_earned_m0'))).alias('points_earned_diff_m0_1')        ,((F.col('a.points_earned_m0')-F.when(F.col('b.points_earned_m0').isNull(),F.lit(0)).otherwise(F.col('b.points_earned_m0')))/F.col('b.points_earned_m0')).alias('points_earned_diff_percent_m0_1')        ,(F.col('g.points_earned_m0_2')-F.when(F.col('h.points_earned_m3_5').isNull(),F.lit(0)).otherwise(F.col('h.points_earned_m3_5'))).alias('points_earned_diff_quarter')        ,((F.col('g.points_earned_m0_2')-F.when(F.col('h.points_earned_m3_5').isNull(),F.lit(0)).otherwise(F.col('h.points_earned_m3_5')))/F.col('h.points_earned_m3_5')).alias('points_earned_diff_percent_quarter')        ,F.col('b.sales_m0').alias('sales_m1')        ,F.col('c.sales_m0').alias('sales_m2')        ,F.col('d.sales_m0').alias('sales_m3')        ,F.col('e.sales_m0').alias('sales_m4')        ,F.col('f.sales_m0').alias('sales_m5')        ,F.col('g.sales_m0_2')        ,F.col('h.sales_m3_5')        ,F.col('i.sales_m0_5')        ,(F.col('b.sales_m0')/F.col('a.sales_m0')).alias('sales_ratio_m0_1')        ,(F.col('c.sales_m0')/F.col('a.sales_m0')).alias('sales_ratio_m0_2')        ,(F.col('d.sales_m0')/F.col('a.sales_m0')).alias('sales_ratio_m0_3')        ,(F.col('e.sales_m0')/F.col('a.sales_m0')).alias('sales_ratio_m0_4')        ,(F.col('f.sales_m0')/F.col('a.sales_m0')).alias('sales_ratio_m0_5')        ,(F.col('a.sales_m0')-F.when(F.col('b.sales_m0').isNull(),0).otherwise(F.col('b.sales_m0'))).alias('sales_diff_m0_1')        ,((F.col('a.sales_m0')-F.col('b.sales_m0'))/F.col('b.sales_m0')).alias('sales_diff_percent_m0_1')        ,(F.col('g.sales_m0_2')-F.when(F.col('h.sales_m3_5').isNull(),0).otherwise(F.col('h.sales_m3_5'))).alias('sales_diff_quarter')        ,((F.col('g.sales_m0_2')-F.col('h.sales_m3_5'))/F.col('h.sales_m3_5')).alias('sales_diff_percent_quarter')        ,F.col('b.transactions_m0').alias('transactions_m1')        ,F.col('c.transactions_m0').alias('transactions_m2')        ,F.col('d.transactions_m0').alias('transactions_m3')        ,F.col('e.transactions_m0').alias('transactions_m4')        ,F.col('f.transactions_m0').alias('transactions_m5')        ,F.col('g.transactions_m0_2')        ,F.col('h.transactions_m3_5')        ,F.col('i.transactions_m0_5')        ,(F.col('b.transactions_m0')/F.col('a.transactions_m0')).alias('transactions_ratio_m0_1')        ,(F.col('c.transactions_m0')/F.col('a.transactions_m0')).alias('transactions_ratio_m0_2')        ,(F.col('d.transactions_m0')/F.col('a.transactions_m0')).alias('transactions_ratio_m0_3')        ,(F.col('e.transactions_m0')/F.col('a.transactions_m0')).alias('transactions_ratio_m0_4')        ,(F.col('f.transactions_m0')/F.col('a.transactions_m0')).alias('transactions_ratio_m0_5')        ,(F.col('a.transactions_m0')-F.when(F.col('b.transactions_m0').isNull(),0).otherwise(F.col('b.transactions_m0'))).alias('transactions_diff_m0_1')        ,((F.col('a.transactions_m0')-F.col('b.transactions_m0'))/F.col('b.transactions_m0')).alias('transactions_diff_percent_m0_1')        ,(F.col('g.transactions_m0_2')-F.when(F.col('h.transactions_m3_5').isNull(),0).otherwise(F.col('h.transactions_m3_5'))).alias('transactions_diff_quarter')        ,((F.col('g.transactions_m0_2')-F.col('h.transactions_m3_5'))/F.col('h.transactions_m3_5')).alias('transactions_diff_percent_quarter')        ,F.col('b.sales_per_transaction_m0').alias('sales_per_transaction_m1')        ,F.col('c.sales_per_transaction_m0').alias('sales_per_transaction_m2')        ,F.col('d.sales_per_transaction_m0').alias('sales_per_transaction_m3')        ,F.col('e.sales_per_transaction_m0').alias('sales_per_transaction_m4')        ,F.col('f.sales_per_transaction_m0').alias('sales_per_transaction_m5')        ,F.col('g.sales_per_transaction_m0_2')        ,F.col('h.sales_per_transaction_m3_5')        ,F.col('i.sales_per_transaction_m0_5')        ,(F.col('b.sales_per_transaction_m0')/F.col('a.sales_per_transaction_m0')).alias('sales_per_transaction_ratio_m0_1')        ,(F.col('c.sales_per_transaction_m0')/F.col('a.sales_per_transaction_m0')).alias('sales_per_transaction_ratio_m0_2')        ,(F.col('d.sales_per_transaction_m0')/F.col('a.sales_per_transaction_m0')).alias('sales_per_transaction_ratio_m0_3')        ,(F.col('e.sales_per_transaction_m0')/F.col('a.sales_per_transaction_m0')).alias('sales_per_transaction_ratio_m0_4')        ,(F.col('f.sales_per_transaction_m0')/F.col('a.sales_per_transaction_m0')).alias('sales_per_transaction_ratio_m0_5')        ,(F.col('a.sales_per_transaction_m0')-F.when(F.col('b.sales_per_transaction_m0').isNull(),0).otherwise(F.col('b.sales_per_transaction_m0'))).alias('sales_per_transaction_diff_m0_1')        ,((F.col('a.sales_per_transaction_m0')-F.col('b.sales_per_transaction_m0'))/F.col('b.sales_per_transaction_m0')).alias('sales_per_transaction_diff_percent_m0_1')        ,(F.col('g.sales_per_transaction_m0_2')-F.when(F.col('h.sales_per_transaction_m3_5').isNull(),0).otherwise(F.col('h.sales_per_transaction_m3_5'))).alias('sales_per_transaction_diff_quarter')        ,((F.col('g.sales_per_transaction_m0_2')-F.col('h.sales_per_transaction_m3_5'))/F.col('h.sales_per_transaction_m3_5')).alias('sales_per_transaction_diff_percent_quarter')        ,F.col('b.number_of_branches_m0').alias('number_of_branches_m1')        ,F.col('c.number_of_branches_m0').alias('number_of_branches_m2')        ,F.col('d.number_of_branches_m0').alias('number_of_branches_m3')        ,F.col('e.number_of_branches_m0').alias('number_of_branches_m4')        ,F.col('f.number_of_branches_m0').alias('number_of_branches_m5')        ,F.col('g.number_of_branches_m0_2')        ,F.col('h.number_of_branches_m3_5')        ,F.col('i.number_of_branches_m0_5')        ,(F.col('b.number_of_branches_m0')/F.col('a.number_of_branches_m0')).alias('number_of_branches_ratio_m0_1')        ,(F.col('c.number_of_branches_m0')/F.col('a.number_of_branches_m0')).alias('number_of_branches_ratio_m0_2')        ,(F.col('d.number_of_branches_m0')/F.col('a.number_of_branches_m0')).alias('number_of_branches_ratio_m0_3')        ,(F.col('e.number_of_branches_m0')/F.col('a.number_of_branches_m0')).alias('number_of_branches_ratio_m0_4')        ,(F.col('f.number_of_branches_m0')/F.col('a.number_of_branches_m0')).alias('number_of_branches_ratio_m0_5')        ,(F.col('a.number_of_branches_m0')-F.when(F.col('b.number_of_branches_m0').isNull(),0).otherwise(F.col('b.number_of_branches_m0'))).alias('number_of_branches_diff_m0_1')        ,((F.col('a.number_of_branches_m0')-F.col('b.number_of_branches_m0'))/F.col('b.number_of_branches_m0')).alias('number_of_branches_diff_percent_m0_1')        ,(F.col('g.number_of_branches_m0_2')-F.when(F.col('h.number_of_branches_m3_5').isNull(),0).otherwise(F.col('h.number_of_branches_m3_5'))).alias('number_of_branches_diff_quarter')        ,((F.col('g.number_of_branches_m0_2')-F.col('h.number_of_branches_m3_5'))/F.col('h.number_of_branches_m3_5')).alias('number_of_branches_diff_percent_quarter')        ,F.col('b.number_of_cards_m0').alias('number_of_cards_m1')        ,F.col('c.number_of_cards_m0').alias('number_of_cards_m2')        ,F.col('d.number_of_cards_m0').alias('number_of_cards_m3')        ,F.col('e.number_of_cards_m0').alias('number_of_cards_m4')        ,F.col('f.number_of_cards_m0').alias('number_of_cards_m5')        ,F.col('g.number_of_cards_m0_2')        ,F.col('h.number_of_cards_m3_5')        ,F.col('i.number_of_cards_m0_5')        ,(F.col('b.number_of_cards_m0')/F.col('a.number_of_cards_m0')).alias('number_of_cards_ratio_m0_1')        ,(F.col('c.number_of_cards_m0')/F.col('a.number_of_cards_m0')).alias('number_of_cards_ratio_m0_2')        ,(F.col('d.number_of_cards_m0')/F.col('a.number_of_cards_m0')).alias('number_of_cards_ratio_m0_3')        ,(F.col('e.number_of_cards_m0')/F.col('a.number_of_cards_m0')).alias('number_of_cards_ratio_m0_4')        ,(F.col('f.number_of_cards_m0')/F.col('a.number_of_cards_m0')).alias('number_of_cards_ratio_m0_5')        ,(F.col('a.number_of_cards_m0')-F.when(F.col('b.number_of_cards_m0').isNull(),0).otherwise(F.col('b.number_of_cards_m0'))).alias('number_of_cards_diff_m0_1')        ,((F.col('a.number_of_cards_m0')-F.col('b.number_of_cards_m0'))/F.col('b.number_of_cards_m0')).alias('number_of_cards_diff_percent_m0_1')        ,(F.col('g.number_of_cards_m0_2')-F.when(F.col('h.number_of_cards_m3_5').isNull(),0).otherwise(F.col('h.number_of_cards_m3_5'))).alias('number_of_cards_diff_quarter')        ,((F.col('g.number_of_cards_m0_2')-F.col('h.number_of_cards_m3_5'))/F.col('h.number_of_cards_m3_5')).alias('number_of_cards_diff_percent_quarter')        ,F.col('g.number_of_months_transacted_m0_2')        ,F.col('h.number_of_months_transacted_m3_5')        ,F.col('i.number_of_months_transacted_m0_5')
       )

                    


# In[ ]:


logger.info("Final Merchant Redemption join")


# In[339]:


df_final_merchant_redemption=df_merchant_issuance.select(['csn','order_no']).alias('a').join(df_merchant_redemption.alias('aa'),(F.col('a.csn')==F.col('aa.csn')) &                                                              (F.col('a.order_no')==F.col('aa.order_no')),'left').join(df_merchant_redemption.alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')==1),'left').join(df_merchant_redemption.alias('c'),(F.col('a.csn')==F.col('c.csn')) &                                                              (F.col('a.order_no')-F.col('c.order_no')==2),'left').join(df_merchant_redemption.alias('d'),(F.col('a.csn')==F.col('d.csn')) &                                                              (F.col('a.order_no')-F.col('d.order_no')==3),'left').join(df_merchant_redemption.alias('e'),(F.col('a.csn')==F.col('e.csn')) &                                                              (F.col('a.order_no')-F.col('e.order_no')==4),'left').join(df_merchant_redemption.alias('f'),(F.col('a.csn')==F.col('f.csn')) &                                                              (F.col('a.order_no')-F.col('f.order_no')==5),'left').join(df_merchant_redemption_0_2months.alias('g'),(F.col('a.csn')==F.col('g.csn')) &                                                              (F.col('a.order_no')==F.col('g.order_no')),'left').join(df_merchant_redemption_3_5months.alias('h'),(F.col('a.csn')==F.col('h.csn')) &                                                              (F.col('a.order_no')==F.col('h.order_no')),'left').join(df_merchant_redemption_0_5months.alias('i'),(F.col('a.csn')==F.col('i.csn')) &                                                              (F.col('a.order_no')==F.col('i.order_no')),'left').select('a.*'        ,'aa.number_of_redemption_branches_m0'        ,'aa.points_redeemed_m0'        ,'aa.sales_redemption_m0'        ,'aa.redemption_transactions_m0'        ,'aa.sales_per_redemption_transaction_m0'        ,F.col('b.points_redeemed_m0').alias('points_redeemed_m1')        ,F.col('c.points_redeemed_m0').alias('points_redeemed_m2')        ,F.col('d.points_redeemed_m0').alias('points_redeemed_m3')        ,F.col('e.points_redeemed_m0').alias('points_redeemed_m4')        ,F.col('f.points_redeemed_m0').alias('points_redeemed_m5')        ,F.col('g.points_redeemed_m0_2')        ,F.col('h.points_redeemed_m3_5')        ,F.col('i.points_redeemed_m0_5')        ,(F.col('b.points_redeemed_m0')/F.col('aa.points_redeemed_m0')).alias('points_redeemed_ratio_m0_1')        ,(F.col('c.points_redeemed_m0')/F.col('aa.points_redeemed_m0')).alias('points_redeemed_ratio_m0_2')        ,(F.col('d.points_redeemed_m0')/F.col('aa.points_redeemed_m0')).alias('points_redeemed_ratio_m0_3')        ,(F.col('e.points_redeemed_m0')/F.col('aa.points_redeemed_m0')).alias('points_redeemed_ratio_m0_4')        ,(F.col('f.points_redeemed_m0')/F.col('aa.points_redeemed_m0')).alias('points_redeemed_ratio_m0_5')        ,(F.col('aa.points_redeemed_m0')-F.when(F.col('b.points_redeemed_m0').isNull(),0).otherwise(F.col('b.points_redeemed_m0'))).alias('points_redeemed_diff_m0_1')        ,((F.col('aa.points_redeemed_m0')-F.col('b.points_redeemed_m0'))/F.col('b.points_redeemed_m0')).alias('points_redeemed_diff_percent_m0_1')        ,(F.col('g.points_redeemed_m0_2')-F.when(F.col('h.points_redeemed_m3_5').isNull(),0).otherwise(F.col('h.points_redeemed_m3_5'))).alias('points_redeemed_diff_quarter')        ,((F.col('g.points_redeemed_m0_2')-F.col('h.points_redeemed_m3_5'))/F.col('h.points_redeemed_m3_5')).alias('points_redeemed_diff_percent_quarter')        ,F.col('b.sales_redemption_m0').alias('sales_redemption_m1')        ,F.col('c.sales_redemption_m0').alias('sales_redemption_m2')        ,F.col('d.sales_redemption_m0').alias('sales_redemption_m3')        ,F.col('e.sales_redemption_m0').alias('sales_redemption_m4')        ,F.col('f.sales_redemption_m0').alias('sales_redemption_m5')        ,F.col('g.sales_redemption_m0_2')        ,F.col('h.sales_redemption_m3_5')        ,F.col('i.sales_redemption_m0_5')        ,(F.col('b.sales_redemption_m0')/F.col('aa.sales_redemption_m0')).alias('sales_redemption_ratio_m0_1')        ,(F.col('c.sales_redemption_m0')/F.col('aa.sales_redemption_m0')).alias('sales_redemption_ratio_m0_2')        ,(F.col('d.sales_redemption_m0')/F.col('aa.sales_redemption_m0')).alias('sales_redemption_ratio_m0_3')        ,(F.col('e.sales_redemption_m0')/F.col('aa.sales_redemption_m0')).alias('sales_redemption_ratio_m0_4')        ,(F.col('f.sales_redemption_m0')/F.col('aa.sales_redemption_m0')).alias('sales_redemption_ratio_m0_5')        ,(F.col('aa.sales_redemption_m0')-F.when(F.col('b.sales_redemption_m0').isNull(),0).otherwise(F.col('b.sales_redemption_m0'))).alias('sales_redemption_diff_m0_1')        ,((F.col('aa.sales_redemption_m0')-F.col('b.sales_redemption_m0'))/F.col('b.sales_redemption_m0')).alias('sales_redemption_diff_percent_m0_1')        ,(F.col('g.sales_redemption_m0_2')-F.when(F.col('h.sales_redemption_m3_5').isNull(),0).otherwise(F.col('h.sales_redemption_m3_5'))).alias('sales_redemption_diff_quarter')        ,((F.col('g.sales_redemption_m0_2')-F.col('h.sales_redemption_m3_5'))/F.col('h.sales_redemption_m3_5')).alias('sales_redemption_diff_percent_quarter')        ,F.col('b.redemption_transactions_m0').alias('redemption_transactions_m1')        ,F.col('c.redemption_transactions_m0').alias('redemption_transactions_m2')        ,F.col('d.redemption_transactions_m0').alias('redemption_transactions_m3')        ,F.col('e.redemption_transactions_m0').alias('redemption_transactions_m4')        ,F.col('f.redemption_transactions_m0').alias('redemption_transactions_m5')        ,F.col('g.redemption_transactions_m0_2')        ,F.col('h.redemption_transactions_m3_5')        ,F.col('i.redemption_transactions_m0_5')        ,(F.col('b.redemption_transactions_m0')/F.col('aa.redemption_transactions_m0')).alias('redemption_transactions_ratio_m0_1')        ,(F.col('c.redemption_transactions_m0')/F.col('aa.redemption_transactions_m0')).alias('redemption_transactions_ratio_m0_2')        ,(F.col('d.redemption_transactions_m0')/F.col('aa.redemption_transactions_m0')).alias('redemption_transactions_ratio_m0_3')        ,(F.col('e.redemption_transactions_m0')/F.col('aa.redemption_transactions_m0')).alias('redemption_transactions_ratio_m0_4')        ,(F.col('f.redemption_transactions_m0')/F.col('aa.redemption_transactions_m0')).alias('redemption_transactions_ratio_m0_5')        ,(F.col('aa.redemption_transactions_m0')-F.when(F.col('b.redemption_transactions_m0').isNull(),0).otherwise(F.col('b.redemption_transactions_m0'))).alias('redemption_transactions_diff_m0_1')        ,((F.col('aa.redemption_transactions_m0')-F.col('b.redemption_transactions_m0'))/F.col('b.redemption_transactions_m0')).alias('redemption_transactions_diff_percent_m0_1')        ,(F.col('g.redemption_transactions_m0_2')-F.when(F.col('h.redemption_transactions_m3_5').isNull(),0).otherwise(F.col('h.redemption_transactions_m3_5'))).alias('redemption_transactions_diff_quarter')        ,((F.col('g.redemption_transactions_m0_2')-F.col('h.redemption_transactions_m3_5'))/F.col('h.redemption_transactions_m3_5')).alias('redemption_transactions_diff_percent_quarter')        ,F.col('b.sales_per_redemption_transaction_m0').alias('sales_per_redemption_transaction_m1')        ,F.col('c.sales_per_redemption_transaction_m0').alias('sales_per_redemption_transaction_m2')        ,F.col('d.sales_per_redemption_transaction_m0').alias('sales_per_redemption_transaction_m3')        ,F.col('e.sales_per_redemption_transaction_m0').alias('sales_per_redemption_transaction_m4')        ,F.col('f.sales_per_redemption_transaction_m0').alias('sales_per_redemption_transaction_m5')        ,F.col('g.sales_per_redemption_transaction_m0_2')        ,F.col('h.sales_per_redemption_transaction_m3_5')        ,F.col('i.sales_per_redemption_transaction_m0_5')        ,(F.col('b.sales_per_redemption_transaction_m0')/F.col('aa.sales_per_redemption_transaction_m0')).alias('sales_per_redemption_transaction_ratio_m0_1')        ,(F.col('c.sales_per_redemption_transaction_m0')/F.col('aa.sales_per_redemption_transaction_m0')).alias('sales_per_redemption_transaction_ratio_m0_2')        ,(F.col('d.sales_per_redemption_transaction_m0')/F.col('aa.sales_per_redemption_transaction_m0')).alias('sales_per_redemption_transaction_ratio_m0_3')        ,(F.col('e.sales_per_redemption_transaction_m0')/F.col('aa.sales_per_redemption_transaction_m0')).alias('sales_per_redemption_transaction_ratio_m0_4')        ,(F.col('f.sales_per_redemption_transaction_m0')/F.col('aa.sales_per_redemption_transaction_m0')).alias('sales_per_redemption_transaction_ratio_m0_5')        ,(F.col('aa.sales_per_redemption_transaction_m0')-F.when(F.col('b.sales_per_redemption_transaction_m0').isNull(),0).otherwise(F.col('b.sales_per_redemption_transaction_m0'))).alias('sales_per_redemption_transaction_diff_m0_1')        ,((F.col('aa.sales_per_redemption_transaction_m0')-F.col('b.sales_per_redemption_transaction_m0'))/F.col('b.sales_per_redemption_transaction_m0')).alias('sales_per_redemption_transaction_diff_percent_m0_1')        ,(F.col('g.sales_per_redemption_transaction_m0_2')-F.when(F.col('h.sales_per_redemption_transaction_m3_5').isNull(),0).otherwise(F.col('h.sales_per_redemption_transaction_m3_5'))).alias('sales_per_redemption_transaction_diff_quarter')        ,((F.col('g.sales_per_redemption_transaction_m0_2')-F.col('h.sales_per_redemption_transaction_m3_5'))/F.col('h.sales_per_redemption_transaction_m3_5')).alias('sales_per_redemption_transaction_diff_percent_quarter')        ,F.col('b.number_of_redemption_branches_m0').alias('number_of_redemption_branches_m1')        ,F.col('c.number_of_redemption_branches_m0').alias('number_of_redemption_branches_m2')        ,F.col('d.number_of_redemption_branches_m0').alias('number_of_redemption_branches_m3')        ,F.col('e.number_of_redemption_branches_m0').alias('number_of_redemption_branches_m4')        ,F.col('f.number_of_redemption_branches_m0').alias('number_of_redemption_branches_m5')        ,F.col('g.number_of_redemption_branches_m0_2')        ,F.col('h.number_of_redemption_branches_m3_5')        ,F.col('i.number_of_redemption_branches_m0_5')        ,(F.col('b.number_of_redemption_branches_m0')/F.col('aa.number_of_redemption_branches_m0')).alias('number_of_redemption_branches_ratio_m0_1')        ,(F.col('c.number_of_redemption_branches_m0')/F.col('aa.number_of_redemption_branches_m0')).alias('number_of_redemption_branches_ratio_m0_2')        ,(F.col('d.number_of_redemption_branches_m0')/F.col('aa.number_of_redemption_branches_m0')).alias('number_of_redemption_branches_ratio_m0_3')        ,(F.col('e.number_of_redemption_branches_m0')/F.col('aa.number_of_redemption_branches_m0')).alias('number_of_redemption_branches_ratio_m0_4')        ,(F.col('f.number_of_redemption_branches_m0')/F.col('aa.number_of_redemption_branches_m0')).alias('number_of_redemption_branches_ratio_m0_5')        ,(F.col('aa.number_of_redemption_branches_m0')-F.when(F.col('b.number_of_redemption_branches_m0').isNull(),0).otherwise(F.col('b.number_of_redemption_branches_m0'))).alias('number_of_redemption_branches_diff_m0_1')        ,((F.col('aa.number_of_redemption_branches_m0')-F.col('b.number_of_redemption_branches_m0'))/F.col('b.number_of_redemption_branches_m0')).alias('number_of_redemption_branches_diff_percent_m0_1')        ,(F.col('g.number_of_redemption_branches_m0_2')-F.when(F.col('h.number_of_redemption_branches_m3_5').isNull(),0).otherwise(F.col('h.number_of_redemption_branches_m3_5'))).alias('number_of_redemption_branches_diff_quarter')        ,((F.col('g.number_of_redemption_branches_m0_2')-F.col('h.number_of_redemption_branches_m3_5'))/F.col('h.number_of_redemption_branches_m3_5')).alias('number_of_redemption_branches_diff_percent_quarter')        ,F.col('g.number_of_months_redeemed_m0_2')        ,F.col('h.number_of_months_redeemed_m3_5')        ,F.col('i.number_of_months_redeemed_m0_5')
       )


# In[340]:


logger.info("Final Other Merchants Issuance Join")


# In[341]:


df_final_other_merchants_issuance=df_merchant_issuance.select(['csn','order_no']).alias('a').join(df_other_merchants_issuance.alias('aa'),(F.col('a.csn')==F.col('aa.csn')) &                                                              (F.col('a.order_no')==F.col('aa.order_no')),'left').join(df_other_merchants_issuance.alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')==1),'left').join(df_other_merchants_issuance.alias('c'),(F.col('a.csn')==F.col('c.csn')) &                                                              (F.col('a.order_no')-F.col('c.order_no')==2),'left').join(df_other_merchants_issuance.alias('d'),(F.col('a.csn')==F.col('d.csn')) &                                                              (F.col('a.order_no')-F.col('d.order_no')==3),'left').join(df_other_merchants_issuance.alias('e'),(F.col('a.csn')==F.col('e.csn')) &                                                              (F.col('a.order_no')-F.col('e.order_no')==4),'left').join(df_other_merchants_issuance.alias('f'),(F.col('a.csn')==F.col('f.csn')) &                                                              (F.col('a.order_no')-F.col('f.order_no')==5),'left').join(df_other_merchants_issuance_0_2months.alias('g'),(F.col('a.csn')==F.col('g.csn')) &                                                              (F.col('a.order_no')==F.col('g.order_no')),'left').join(df_other_merchants_issuance_3_5months.alias('h'),(F.col('a.csn')==F.col('h.csn')) &                                                              (F.col('a.order_no')==F.col('h.order_no')),'left').join(df_other_merchants_issuance_0_5months.alias('i'),(F.col('a.csn')==F.col('i.csn')) &                                                              (F.col('a.order_no')==F.col('i.order_no')),'left').select('a.*'        ,'aa.number_of_branches_other_merchants_m0'        ,'aa.points_earned_other_merchants_m0'        ,'aa.sales_other_merchants_m0'        ,'aa.transactions_other_merchants_m0'        ,'aa.sales_per_transaction_other_merchants_m0'        ,'aa.number_of_cards_other_merchants_m0'        ,'aa.number_of_merchants_other_merchants_m0'        ,F.col('b.number_of_merchants_other_merchants_m0').alias('number_of_merchants_other_merchants_m1')        ,F.col('c.number_of_merchants_other_merchants_m0').alias('number_of_merchants_other_merchants_m2')        ,F.col('d.number_of_merchants_other_merchants_m0').alias('number_of_merchants_other_merchants_m3')        ,F.col('e.number_of_merchants_other_merchants_m0').alias('number_of_merchants_other_merchants_m4')        ,F.col('f.number_of_merchants_other_merchants_m0').alias('number_of_merchants_other_merchants_m5')        ,F.col('g.number_of_merchants_other_merchants_m0_2')        ,F.col('h.number_of_merchants_other_merchants_m3_5')        ,F.col('i.number_of_merchants_other_merchants_m0_5')        ,(F.col('b.number_of_merchants_other_merchants_m0')/F.col('aa.number_of_merchants_other_merchants_m0')).alias('number_of_merchants_other_merchants_ratio_m0_1')        ,(F.col('c.number_of_merchants_other_merchants_m0')/F.col('aa.number_of_merchants_other_merchants_m0')).alias('number_of_merchants_other_merchants_ratio_m0_2')        ,(F.col('d.number_of_merchants_other_merchants_m0')/F.col('aa.number_of_merchants_other_merchants_m0')).alias('number_of_merchants_other_merchants_ratio_m0_3')        ,(F.col('e.number_of_merchants_other_merchants_m0')/F.col('aa.number_of_merchants_other_merchants_m0')).alias('number_of_merchants_other_merchants_ratio_m0_4')        ,(F.col('f.number_of_merchants_other_merchants_m0')/F.col('aa.number_of_merchants_other_merchants_m0')).alias('number_of_merchants_other_merchants_ratio_m0_5')        ,(F.col('aa.number_of_merchants_other_merchants_m0')-F.when(F.col('b.number_of_merchants_other_merchants_m0').isNull(),0).otherwise(F.col('b.number_of_merchants_other_merchants_m0'))).alias('number_of_merchants_other_merchants_diff_m0_1')        ,((F.col('aa.number_of_merchants_other_merchants_m0')-F.col('b.number_of_merchants_other_merchants_m0'))/F.col('b.number_of_merchants_other_merchants_m0')).alias('number_of_merchants_other_merchants_diff_percent_m0_1')        ,(F.col('g.number_of_merchants_other_merchants_m0_2')-F.when(F.col('h.number_of_merchants_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.number_of_merchants_other_merchants_m3_5'))).alias('number_of_merchants_other_merchants_diff_quarter')        ,((F.col('g.number_of_merchants_other_merchants_m0_2')-F.col('h.number_of_merchants_other_merchants_m3_5'))/F.col('h.number_of_merchants_other_merchants_m3_5')).alias('number_of_merchants_other_merchants_diff_percent_quarter')        ,F.col('b.sales_other_merchants_m0').alias('sales_other_merchants_m1')        ,F.col('c.sales_other_merchants_m0').alias('sales_other_merchants_m2')        ,F.col('d.sales_other_merchants_m0').alias('sales_other_merchants_m3')        ,F.col('e.sales_other_merchants_m0').alias('sales_other_merchants_m4')        ,F.col('f.sales_other_merchants_m0').alias('sales_other_merchants_m5')        ,F.col('g.sales_other_merchants_m0_2')        ,F.col('h.sales_other_merchants_m3_5')        ,F.col('i.sales_other_merchants_m0_5')        ,(F.col('b.sales_other_merchants_m0')/F.col('aa.sales_other_merchants_m0')).alias('sales_other_merchants_ratio_m0_1')        ,(F.col('c.sales_other_merchants_m0')/F.col('aa.sales_other_merchants_m0')).alias('sales_other_merchants_ratio_m0_2')        ,(F.col('d.sales_other_merchants_m0')/F.col('aa.sales_other_merchants_m0')).alias('sales_other_merchants_ratio_m0_3')        ,(F.col('e.sales_other_merchants_m0')/F.col('aa.sales_other_merchants_m0')).alias('sales_other_merchants_ratio_m0_4')        ,(F.col('f.sales_other_merchants_m0')/F.col('aa.sales_other_merchants_m0')).alias('sales_other_merchants_ratio_m0_5')        ,(F.col('aa.sales_other_merchants_m0')-F.when(F.col('b.sales_other_merchants_m0').isNull(),0).otherwise(F.col('b.sales_other_merchants_m0'))).alias('sales_other_merchants_diff_m0_1')        ,((F.col('aa.sales_other_merchants_m0')-F.col('b.sales_other_merchants_m0'))/F.col('b.sales_other_merchants_m0')).alias('sales_other_merchants_diff_percent_m0_1')        ,(F.col('g.sales_other_merchants_m0_2')-F.when(F.col('h.sales_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.sales_other_merchants_m3_5'))).alias('sales_other_merchants_diff_quarter')        ,((F.col('g.sales_other_merchants_m0_2')-F.col('h.sales_other_merchants_m3_5'))/F.col('h.sales_other_merchants_m3_5')).alias('sales_other_merchants_diff_percent_quarter')        ,F.col('b.points_earned_other_merchants_m0').alias('points_earned_other_merchants_m1')        ,F.col('c.points_earned_other_merchants_m0').alias('points_earned_other_merchants_m2')        ,F.col('d.points_earned_other_merchants_m0').alias('points_earned_other_merchants_m3')        ,F.col('e.points_earned_other_merchants_m0').alias('points_earned_other_merchants_m4')        ,F.col('f.points_earned_other_merchants_m0').alias('points_earned_other_merchants_m5')        ,F.col('g.points_earned_other_merchants_m0_2')        ,F.col('h.points_earned_other_merchants_m3_5')        ,F.col('i.points_earned_other_merchants_m0_5')        ,(F.col('b.points_earned_other_merchants_m0')/F.col('aa.points_earned_other_merchants_m0')).alias('points_earned_other_merchants_ratio_m0_1')        ,(F.col('c.points_earned_other_merchants_m0')/F.col('aa.points_earned_other_merchants_m0')).alias('points_earned_other_merchants_ratio_m0_2')        ,(F.col('d.points_earned_other_merchants_m0')/F.col('aa.points_earned_other_merchants_m0')).alias('points_earned_other_merchants_ratio_m0_3')        ,(F.col('e.points_earned_other_merchants_m0')/F.col('aa.points_earned_other_merchants_m0')).alias('points_earned_other_merchants_ratio_m0_4')        ,(F.col('f.points_earned_other_merchants_m0')/F.col('aa.points_earned_other_merchants_m0')).alias('points_earned_other_merchants_ratio_m0_5')        ,(F.col('aa.points_earned_other_merchants_m0')-F.when(F.col('b.points_earned_other_merchants_m0').isNull(),0).otherwise(F.col('b.points_earned_other_merchants_m0'))).alias('points_earned_other_merchants_diff_m0_1')        ,((F.col('aa.points_earned_other_merchants_m0')-F.col('b.points_earned_other_merchants_m0'))/F.col('b.points_earned_other_merchants_m0')).alias('points_earned_other_merchants_diff_percent_m0_1')        ,(F.col('g.points_earned_other_merchants_m0_2')-F.when(F.col('h.points_earned_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.points_earned_other_merchants_m3_5'))).alias('points_earned_other_merchants_diff_quarter')        ,((F.col('g.points_earned_other_merchants_m0_2')-F.col('h.points_earned_other_merchants_m3_5'))/F.col('h.points_earned_other_merchants_m3_5')).alias('points_earned_other_merchants_diff_percent_quarter')        ,F.col('b.transactions_other_merchants_m0').alias('transactions_other_merchants_m1')        ,F.col('c.transactions_other_merchants_m0').alias('transactions_other_merchants_m2')        ,F.col('d.transactions_other_merchants_m0').alias('transactions_other_merchants_m3')        ,F.col('e.transactions_other_merchants_m0').alias('transactions_other_merchants_m4')        ,F.col('f.transactions_other_merchants_m0').alias('transactions_other_merchants_m5')        ,F.col('g.transactions_other_merchants_m0_2')        ,F.col('h.transactions_other_merchants_m3_5')        ,F.col('i.transactions_other_merchants_m0_5')        ,(F.col('b.transactions_other_merchants_m0')/F.col('aa.transactions_other_merchants_m0')).alias('transactions_other_merchants_ratio_m0_1')        ,(F.col('c.transactions_other_merchants_m0')/F.col('aa.transactions_other_merchants_m0')).alias('transactions_other_merchants_ratio_m0_2')        ,(F.col('d.transactions_other_merchants_m0')/F.col('aa.transactions_other_merchants_m0')).alias('transactions_other_merchants_ratio_m0_3')        ,(F.col('e.transactions_other_merchants_m0')/F.col('aa.transactions_other_merchants_m0')).alias('transactions_other_merchants_ratio_m0_4')        ,(F.col('f.transactions_other_merchants_m0')/F.col('aa.transactions_other_merchants_m0')).alias('transactions_other_merchants_ratio_m0_5')        ,(F.col('aa.transactions_other_merchants_m0')-F.when(F.col('b.transactions_other_merchants_m0').isNull(),0).otherwise(F.col('b.transactions_other_merchants_m0'))).alias('transactions_other_merchants_diff_m0_1')        ,((F.col('aa.transactions_other_merchants_m0')-F.col('b.transactions_other_merchants_m0'))/F.col('b.transactions_other_merchants_m0')).alias('transactions_other_merchants_diff_percent_m0_1')        ,(F.col('g.transactions_other_merchants_m0_2')-F.when(F.col('h.transactions_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.transactions_other_merchants_m3_5'))).alias('transactions_other_merchants_diff_quarter')        ,((F.col('g.transactions_other_merchants_m0_2')-F.col('h.transactions_other_merchants_m3_5'))/F.col('h.transactions_other_merchants_m3_5')).alias('transactions_other_merchants_diff_percent_quarter')        ,F.col('b.sales_per_transaction_other_merchants_m0').alias('sales_per_transaction_other_merchants_m1')        ,F.col('c.sales_per_transaction_other_merchants_m0').alias('sales_per_transaction_other_merchants_m2')        ,F.col('d.sales_per_transaction_other_merchants_m0').alias('sales_per_transaction_other_merchants_m3')        ,F.col('e.sales_per_transaction_other_merchants_m0').alias('sales_per_transaction_other_merchants_m4')        ,F.col('f.sales_per_transaction_other_merchants_m0').alias('sales_per_transaction_other_merchants_m5')        ,F.col('g.sales_per_transaction_other_merchants_m0_2')        ,F.col('h.sales_per_transaction_other_merchants_m3_5')        ,F.col('i.sales_per_transaction_other_merchants_m0_5')        ,(F.col('b.sales_per_transaction_other_merchants_m0')/F.col('aa.sales_per_transaction_other_merchants_m0')).alias('sales_per_transaction_other_merchants_ratio_m0_1')        ,(F.col('c.sales_per_transaction_other_merchants_m0')/F.col('aa.sales_per_transaction_other_merchants_m0')).alias('sales_per_transaction_other_merchants_ratio_m0_2')        ,(F.col('d.sales_per_transaction_other_merchants_m0')/F.col('aa.sales_per_transaction_other_merchants_m0')).alias('sales_per_transaction_other_merchants_ratio_m0_3')        ,(F.col('e.sales_per_transaction_other_merchants_m0')/F.col('aa.sales_per_transaction_other_merchants_m0')).alias('sales_per_transaction_other_merchants_ratio_m0_4')        ,(F.col('f.sales_per_transaction_other_merchants_m0')/F.col('aa.sales_per_transaction_other_merchants_m0')).alias('sales_per_transaction_other_merchants_ratio_m0_5')        ,(F.col('aa.sales_per_transaction_other_merchants_m0')-F.when(F.col('b.sales_per_transaction_other_merchants_m0').isNull(),0).otherwise(F.col('b.sales_per_transaction_other_merchants_m0'))).alias('sales_per_transaction_other_merchants_diff_m0_1')        ,((F.col('aa.sales_per_transaction_other_merchants_m0')-F.col('b.sales_per_transaction_other_merchants_m0'))/F.col('b.sales_per_transaction_other_merchants_m0')).alias('sales_per_transaction_other_merchants_diff_percent_m0_1')        ,(F.col('g.sales_per_transaction_other_merchants_m0_2')-F.when(F.col('h.sales_per_transaction_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.sales_per_transaction_other_merchants_m3_5'))).alias('sales_per_transaction_other_merchants_diff_quarter')        ,((F.col('g.sales_per_transaction_other_merchants_m0_2')-F.col('h.sales_per_transaction_other_merchants_m3_5'))/F.col('h.sales_per_transaction_other_merchants_m3_5')).alias('sales_per_transaction_other_merchants_diff_percent_quarter')        ,F.col('b.number_of_branches_other_merchants_m0').alias('number_of_branches_other_merchants_m1')        ,F.col('c.number_of_branches_other_merchants_m0').alias('number_of_branches_other_merchants_m2')        ,F.col('d.number_of_branches_other_merchants_m0').alias('number_of_branches_other_merchants_m3')        ,F.col('e.number_of_branches_other_merchants_m0').alias('number_of_branches_other_merchants_m4')        ,F.col('f.number_of_branches_other_merchants_m0').alias('number_of_branches_other_merchants_m5')        ,F.col('g.number_of_branches_other_merchants_m0_2')        ,F.col('h.number_of_branches_other_merchants_m3_5')        ,F.col('i.number_of_branches_other_merchants_m0_5')        ,(F.col('b.number_of_branches_other_merchants_m0')/F.col('aa.number_of_branches_other_merchants_m0')).alias('number_of_branches_other_merchants_ratio_m0_1')        ,(F.col('c.number_of_branches_other_merchants_m0')/F.col('aa.number_of_branches_other_merchants_m0')).alias('number_of_branches_other_merchants_ratio_m0_2')        ,(F.col('d.number_of_branches_other_merchants_m0')/F.col('aa.number_of_branches_other_merchants_m0')).alias('number_of_branches_other_merchants_ratio_m0_3')        ,(F.col('e.number_of_branches_other_merchants_m0')/F.col('aa.number_of_branches_other_merchants_m0')).alias('number_of_branches_other_merchants_ratio_m0_4')        ,(F.col('f.number_of_branches_other_merchants_m0')/F.col('aa.number_of_branches_other_merchants_m0')).alias('number_of_branches_other_merchants_ratio_m0_5')        ,(F.col('aa.number_of_branches_other_merchants_m0')-F.when(F.col('b.number_of_branches_other_merchants_m0').isNull(),0).otherwise(F.col('b.number_of_branches_other_merchants_m0'))).alias('number_of_branches_other_merchants_diff_m0_1')        ,((F.col('aa.number_of_branches_other_merchants_m0')-F.col('b.number_of_branches_other_merchants_m0'))/F.col('b.number_of_branches_other_merchants_m0')).alias('number_of_branches_other_merchants_diff_percent_m0_1')        ,(F.col('g.number_of_branches_other_merchants_m0_2')-F.when(F.col('h.number_of_branches_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.number_of_branches_other_merchants_m3_5'))).alias('number_of_branches_other_merchants_diff_quarter')        ,((F.col('g.number_of_branches_other_merchants_m0_2')-F.col('h.number_of_branches_other_merchants_m3_5'))/F.col('h.number_of_branches_other_merchants_m3_5')).alias('number_of_branches_other_merchants_diff_percent_quarter')        ,F.col('b.number_of_cards_other_merchants_m0').alias('number_of_cards_other_merchants_m1')        ,F.col('c.number_of_cards_other_merchants_m0').alias('number_of_cards_other_merchants_m2')        ,F.col('d.number_of_cards_other_merchants_m0').alias('number_of_cards_other_merchants_m3')        ,F.col('e.number_of_cards_other_merchants_m0').alias('number_of_cards_other_merchants_m4')        ,F.col('f.number_of_cards_other_merchants_m0').alias('number_of_cards_other_merchants_m5')        ,F.col('g.number_of_cards_other_merchants_m0_2')        ,F.col('h.number_of_cards_other_merchants_m3_5')        ,F.col('i.number_of_cards_other_merchants_m0_5')        ,(F.col('b.number_of_cards_other_merchants_m0')/F.col('aa.number_of_cards_other_merchants_m0')).alias('number_of_cards_other_merchants_ratio_m0_1')        ,(F.col('c.number_of_cards_other_merchants_m0')/F.col('aa.number_of_cards_other_merchants_m0')).alias('number_of_cards_other_merchants_ratio_m0_2')        ,(F.col('d.number_of_cards_other_merchants_m0')/F.col('aa.number_of_cards_other_merchants_m0')).alias('number_of_cards_other_merchants_ratio_m0_3')        ,(F.col('e.number_of_cards_other_merchants_m0')/F.col('aa.number_of_cards_other_merchants_m0')).alias('number_of_cards_other_merchants_ratio_m0_4')        ,(F.col('f.number_of_cards_other_merchants_m0')/F.col('aa.number_of_cards_other_merchants_m0')).alias('number_of_cards_other_merchants_ratio_m0_5')        ,(F.col('aa.number_of_cards_other_merchants_m0')-F.when(F.col('b.number_of_cards_other_merchants_m0').isNull(),0).otherwise(F.col('b.number_of_cards_other_merchants_m0'))).alias('number_of_cards_other_merchants_diff_m0_1')        ,((F.col('aa.number_of_cards_other_merchants_m0')-F.col('b.number_of_cards_other_merchants_m0'))/F.col('b.number_of_cards_other_merchants_m0')).alias('number_of_cards_other_merchants_diff_percent_m0_1')        ,(F.col('g.number_of_cards_other_merchants_m0_2')-F.when(F.col('h.number_of_cards_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.number_of_cards_other_merchants_m3_5'))).alias('number_of_cards_other_merchants_diff_quarter')        ,((F.col('g.number_of_cards_other_merchants_m0_2')-F.col('h.number_of_cards_other_merchants_m3_5'))/F.col('h.number_of_cards_other_merchants_m3_5')).alias('number_of_cards_other_merchants_diff_percent_quarter')        ,F.col('g.number_of_months_transacted_other_merchants_m0_2')        ,F.col('h.number_of_months_transacted_other_merchants_m3_5')        ,F.col('i.number_of_months_transacted_other_merchants_m0_5')
)


# In[342]:


logger.info("Final Other Merchants Redemption Join")


# In[345]:


df_final_other_merchants_redemption=df_merchant_issuance.select(['csn','order_no']).alias('a').join(df_other_merchants_redemption.alias('aa'),(F.col('a.csn')==F.col('aa.csn')) &                                                              (F.col('a.order_no')==F.col('aa.order_no')),'left').join(df_other_merchants_redemption.alias('b'),(F.col('a.csn')==F.col('b.csn')) &                                                              (F.col('a.order_no')-F.col('b.order_no')==1),'left').join(df_other_merchants_redemption.alias('c'),(F.col('a.csn')==F.col('c.csn')) &                                                              (F.col('a.order_no')-F.col('c.order_no')==2),'left').join(df_other_merchants_redemption.alias('d'),(F.col('a.csn')==F.col('d.csn')) &                                                              (F.col('a.order_no')-F.col('d.order_no')==3),'left').join(df_other_merchants_redemption.alias('e'),(F.col('a.csn')==F.col('e.csn')) &                                                              (F.col('a.order_no')-F.col('e.order_no')==4),'left').join(df_other_merchants_redemption.alias('f'),(F.col('a.csn')==F.col('f.csn')) &                                                              (F.col('a.order_no')-F.col('f.order_no')==5),'left').join(df_other_merchants_redemption_0_2months.alias('g'),(F.col('a.csn')==F.col('g.csn')) &                                                              (F.col('a.order_no')==F.col('g.order_no')),'left').join(df_other_merchants_redemption_3_5months.alias('h'),(F.col('a.csn')==F.col('h.csn')) &                                                              (F.col('a.order_no')==F.col('h.order_no')),'left').join(df_other_merchants_redemption_0_5months.alias('i'),(F.col('a.csn')==F.col('i.csn')) &                                                              (F.col('a.order_no')==F.col('i.order_no')),'left').select('a.*'        ,'aa.number_of_redemption_branches_other_merchants_m0'        ,'aa.points_redeemed_other_merchants_m0'        ,'aa.sales_redemption_other_merchants_m0'        ,'aa.redemption_transactions_other_merchants_m0'        ,'aa.sales_per_redemption_transaction_other_merchants_m0'        ,'aa.number_of_merchants_redeemed_other_merchants_m0'        ,F.col('b.number_of_merchants_redeemed_other_merchants_m0').alias('number_of_merchants_redeemed_other_merchants_m1')        ,F.col('c.number_of_merchants_redeemed_other_merchants_m0').alias('number_of_merchants_redeemed_other_merchants_m2')        ,F.col('d.number_of_merchants_redeemed_other_merchants_m0').alias('number_of_merchants_redeemed_other_merchants_m3')        ,F.col('e.number_of_merchants_redeemed_other_merchants_m0').alias('number_of_merchants_redeemed_other_merchants_m4')        ,F.col('f.number_of_merchants_redeemed_other_merchants_m0').alias('number_of_merchants_redeemed_other_merchants_m5')        ,F.col('g.number_of_merchants_redeemed_other_merchants_m0_2')        ,F.col('h.number_of_merchants_redeemed_other_merchants_m3_5')        ,F.col('i.number_of_merchants_redeemed_other_merchants_m0_5')        ,(F.col('b.number_of_merchants_redeemed_other_merchants_m0')/F.col('aa.number_of_merchants_redeemed_other_merchants_m0')).alias('number_of_merchants_redeemed_other_merchants_ratio_m0_1')        ,(F.col('c.number_of_merchants_redeemed_other_merchants_m0')/F.col('aa.number_of_merchants_redeemed_other_merchants_m0')).alias('number_of_merchants_redeemed_other_merchants_ratio_m0_2')        ,(F.col('d.number_of_merchants_redeemed_other_merchants_m0')/F.col('aa.number_of_merchants_redeemed_other_merchants_m0')).alias('number_of_merchants_redeemed_other_merchants_ratio_m0_3')        ,(F.col('e.number_of_merchants_redeemed_other_merchants_m0')/F.col('aa.number_of_merchants_redeemed_other_merchants_m0')).alias('number_of_merchants_redeemed_other_merchants_ratio_m0_4')        ,(F.col('f.number_of_merchants_redeemed_other_merchants_m0')/F.col('aa.number_of_merchants_redeemed_other_merchants_m0')).alias('number_of_merchants_redeemed_other_merchants_ratio_m0_5')        ,(F.col('aa.number_of_merchants_redeemed_other_merchants_m0')-F.when(F.col('b.number_of_merchants_redeemed_other_merchants_m0').isNull(),0).otherwise(F.col('b.number_of_merchants_redeemed_other_merchants_m0'))).alias('number_of_merchants_redeemed_other_merchants_diff_m0_1')        ,((F.col('aa.number_of_merchants_redeemed_other_merchants_m0')-F.col('b.number_of_merchants_redeemed_other_merchants_m0'))/F.col('b.number_of_merchants_redeemed_other_merchants_m0')).alias('number_of_merchants_redeemed_other_merchants_diff_percent_m0_1')        ,(F.col('g.number_of_merchants_redeemed_other_merchants_m0_2')-F.when(F.col('h.number_of_merchants_redeemed_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.number_of_merchants_redeemed_other_merchants_m3_5'))).alias('number_of_merchants_redeemed_other_merchants_diff_quarter')        ,((F.col('g.number_of_merchants_redeemed_other_merchants_m0_2')-F.col('h.number_of_merchants_redeemed_other_merchants_m3_5'))/F.col('h.number_of_merchants_redeemed_other_merchants_m3_5')).alias('number_of_merchants_redeemed_other_merchants_diff_percent_quarter')        ,F.col('b.points_redeemed_other_merchants_m0').alias('points_redeemed_other_merchants_m1')        ,F.col('c.points_redeemed_other_merchants_m0').alias('points_redeemed_other_merchants_m2')        ,F.col('d.points_redeemed_other_merchants_m0').alias('points_redeemed_other_merchants_m3')        ,F.col('e.points_redeemed_other_merchants_m0').alias('points_redeemed_other_merchants_m4')        ,F.col('f.points_redeemed_other_merchants_m0').alias('points_redeemed_other_merchants_m5')        ,F.col('g.points_redeemed_other_merchants_m0_2')        ,F.col('h.points_redeemed_other_merchants_m3_5')        ,F.col('i.points_redeemed_other_merchants_m0_5')        ,(F.col('b.points_redeemed_other_merchants_m0')/F.col('aa.points_redeemed_other_merchants_m0')).alias('points_redeemed_other_merchants_ratio_m0_1')        ,(F.col('c.points_redeemed_other_merchants_m0')/F.col('aa.points_redeemed_other_merchants_m0')).alias('points_redeemed_other_merchants_ratio_m0_2')        ,(F.col('d.points_redeemed_other_merchants_m0')/F.col('aa.points_redeemed_other_merchants_m0')).alias('points_redeemed_other_merchants_ratio_m0_3')        ,(F.col('e.points_redeemed_other_merchants_m0')/F.col('aa.points_redeemed_other_merchants_m0')).alias('points_redeemed_other_merchants_ratio_m0_4')        ,(F.col('f.points_redeemed_other_merchants_m0')/F.col('aa.points_redeemed_other_merchants_m0')).alias('points_redeemed_other_merchants_ratio_m0_5')        ,(F.col('aa.points_redeemed_other_merchants_m0')-F.when(F.col('b.points_redeemed_other_merchants_m0').isNull(),0).otherwise(F.col('b.points_redeemed_other_merchants_m0'))).alias('points_redeemed_other_merchants_diff_m0_1')        ,((F.col('aa.points_redeemed_other_merchants_m0')-F.col('b.points_redeemed_other_merchants_m0'))/F.col('b.points_redeemed_other_merchants_m0')).alias('points_redeemed_other_merchants_diff_percent_m0_1')        ,(F.col('g.points_redeemed_other_merchants_m0_2')-F.when(F.col('h.points_redeemed_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.points_redeemed_other_merchants_m3_5'))).alias('points_redeemed_other_merchants_diff_quarter')        ,((F.col('g.points_redeemed_other_merchants_m0_2')-F.col('h.points_redeemed_other_merchants_m3_5'))/F.col('h.points_redeemed_other_merchants_m3_5')).alias('points_redeemed_other_merchants_diff_percent_quarter')        ,F.col('b.sales_redemption_other_merchants_m0').alias('sales_redemption_other_merchants_m1')        ,F.col('c.sales_redemption_other_merchants_m0').alias('sales_redemption_other_merchants_m2')        ,F.col('d.sales_redemption_other_merchants_m0').alias('sales_redemption_other_merchants_m3')        ,F.col('e.sales_redemption_other_merchants_m0').alias('sales_redemption_other_merchants_m4')        ,F.col('f.sales_redemption_other_merchants_m0').alias('sales_redemption_other_merchants_m5')        ,F.col('g.sales_redemption_other_merchants_m0_2')        ,F.col('h.sales_redemption_other_merchants_m3_5')        ,F.col('i.sales_redemption_other_merchants_m0_5')        ,(F.col('b.sales_redemption_other_merchants_m0')/F.col('aa.sales_redemption_other_merchants_m0')).alias('sales_redemption_other_merchants_ratio_m0_1')        ,(F.col('c.sales_redemption_other_merchants_m0')/F.col('aa.sales_redemption_other_merchants_m0')).alias('sales_redemption_other_merchants_ratio_m0_2')        ,(F.col('d.sales_redemption_other_merchants_m0')/F.col('aa.sales_redemption_other_merchants_m0')).alias('sales_redemption_other_merchants_ratio_m0_3')        ,(F.col('e.sales_redemption_other_merchants_m0')/F.col('aa.sales_redemption_other_merchants_m0')).alias('sales_redemption_other_merchants_ratio_m0_4')        ,(F.col('f.sales_redemption_other_merchants_m0')/F.col('aa.sales_redemption_other_merchants_m0')).alias('sales_redemption_other_merchants_ratio_m0_5')        ,(F.col('aa.sales_redemption_other_merchants_m0')-F.when(F.col('b.sales_redemption_other_merchants_m0').isNull(),0).otherwise(F.col('b.sales_redemption_other_merchants_m0'))).alias('sales_redemption_other_merchants_diff_m0_1')        ,((F.col('aa.sales_redemption_other_merchants_m0')-F.col('b.sales_redemption_other_merchants_m0'))/F.col('b.sales_redemption_other_merchants_m0')).alias('sales_redemption_other_merchants_diff_percent_m0_1')        ,(F.col('g.sales_redemption_other_merchants_m0_2')-F.when(F.col('h.sales_redemption_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.sales_redemption_other_merchants_m3_5'))).alias('sales_redemption_other_merchants_diff_quarter')        ,((F.col('g.sales_redemption_other_merchants_m0_2')-F.col('h.sales_redemption_other_merchants_m3_5'))/F.col('h.sales_redemption_other_merchants_m3_5')).alias('sales_redemption_other_merchants_diff_percent_quarter')        ,F.col('b.redemption_transactions_other_merchants_m0').alias('redemption_transactions_other_merchants_m1')        ,F.col('c.redemption_transactions_other_merchants_m0').alias('redemption_transactions_other_merchants_m2')        ,F.col('d.redemption_transactions_other_merchants_m0').alias('redemption_transactions_other_merchants_m3')        ,F.col('e.redemption_transactions_other_merchants_m0').alias('redemption_transactions_other_merchants_m4')        ,F.col('f.redemption_transactions_other_merchants_m0').alias('redemption_transactions_other_merchants_m5')        ,F.col('g.redemption_transactions_other_merchants_m0_2')        ,F.col('h.redemption_transactions_other_merchants_m3_5')        ,F.col('i.redemption_transactions_other_merchants_m0_5')        ,(F.col('b.redemption_transactions_other_merchants_m0')/F.col('aa.redemption_transactions_other_merchants_m0')).alias('redemption_transactions_other_merchants_ratio_m0_1')        ,(F.col('c.redemption_transactions_other_merchants_m0')/F.col('aa.redemption_transactions_other_merchants_m0')).alias('redemption_transactions_other_merchants_ratio_m0_2')        ,(F.col('d.redemption_transactions_other_merchants_m0')/F.col('aa.redemption_transactions_other_merchants_m0')).alias('redemption_transactions_other_merchants_ratio_m0_3')        ,(F.col('e.redemption_transactions_other_merchants_m0')/F.col('aa.redemption_transactions_other_merchants_m0')).alias('redemption_transactions_other_merchants_ratio_m0_4')        ,(F.col('f.redemption_transactions_other_merchants_m0')/F.col('aa.redemption_transactions_other_merchants_m0')).alias('redemption_transactions_other_merchants_ratio_m0_5')        ,(F.col('aa.redemption_transactions_other_merchants_m0')-F.when(F.col('b.redemption_transactions_other_merchants_m0').isNull(),0).otherwise(F.col('b.redemption_transactions_other_merchants_m0'))).alias('redemption_transactions_other_merchants_diff_m0_1')        ,((F.col('aa.redemption_transactions_other_merchants_m0')-F.col('b.redemption_transactions_other_merchants_m0'))/F.col('b.redemption_transactions_other_merchants_m0')).alias('redemption_transactions_other_merchants_diff_percent_m0_1')        ,(F.col('g.redemption_transactions_other_merchants_m0_2')-F.when(F.col('h.redemption_transactions_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.redemption_transactions_other_merchants_m3_5'))).alias('redemption_transactions_other_merchants_diff_quarter')        ,((F.col('g.redemption_transactions_other_merchants_m0_2')-F.col('h.redemption_transactions_other_merchants_m3_5'))/F.col('h.redemption_transactions_other_merchants_m3_5')).alias('redemption_transactions_other_merchants_diff_percent_quarter')        ,F.col('b.sales_per_redemption_transaction_other_merchants_m0').alias('sales_per_redemption_transaction_other_merchants_m1')        ,F.col('c.sales_per_redemption_transaction_other_merchants_m0').alias('sales_per_redemption_transaction_other_merchants_m2')        ,F.col('d.sales_per_redemption_transaction_other_merchants_m0').alias('sales_per_redemption_transaction_other_merchants_m3')        ,F.col('e.sales_per_redemption_transaction_other_merchants_m0').alias('sales_per_redemption_transaction_other_merchants_m4')        ,F.col('f.sales_per_redemption_transaction_other_merchants_m0').alias('sales_per_redemption_transaction_other_merchants_m5')        ,F.col('g.sales_per_redemption_transaction_other_merchants_m0_2')        ,F.col('h.sales_per_redemption_transaction_other_merchants_m3_5')        ,F.col('i.sales_per_redemption_transaction_other_merchants_m0_5')        ,(F.col('b.sales_per_redemption_transaction_other_merchants_m0')/F.col('aa.sales_per_redemption_transaction_other_merchants_m0')).alias('sales_per_redemption_transaction_other_merchants_ratio_m0_1')        ,(F.col('c.sales_per_redemption_transaction_other_merchants_m0')/F.col('aa.sales_per_redemption_transaction_other_merchants_m0')).alias('sales_per_redemption_transaction_other_merchants_ratio_m0_2')        ,(F.col('d.sales_per_redemption_transaction_other_merchants_m0')/F.col('aa.sales_per_redemption_transaction_other_merchants_m0')).alias('sales_per_redemption_transaction_other_merchants_ratio_m0_3')        ,(F.col('e.sales_per_redemption_transaction_other_merchants_m0')/F.col('aa.sales_per_redemption_transaction_other_merchants_m0')).alias('sales_per_redemption_transaction_other_merchants_ratio_m0_4')        ,(F.col('f.sales_per_redemption_transaction_other_merchants_m0')/F.col('aa.sales_per_redemption_transaction_other_merchants_m0')).alias('sales_per_redemption_transaction_other_merchants_ratio_m0_5')        ,(F.col('aa.sales_per_redemption_transaction_other_merchants_m0')-F.when(F.col('b.sales_per_redemption_transaction_other_merchants_m0').isNull(),0).otherwise(F.col('b.sales_per_redemption_transaction_other_merchants_m0'))).alias('sales_per_redemption_transaction_other_merchants_diff_m0_1')        ,((F.col('aa.sales_per_redemption_transaction_other_merchants_m0')-F.col('b.sales_per_redemption_transaction_other_merchants_m0'))/F.col('b.sales_per_redemption_transaction_other_merchants_m0')).alias('sales_per_redemption_transaction_other_merchants_diff_percent_m0_1')        ,(F.col('g.sales_per_redemption_transaction_other_merchants_m0_2')-F.when(F.col('h.sales_per_redemption_transaction_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.sales_per_redemption_transaction_other_merchants_m3_5'))).alias('sales_per_redemption_transaction_other_merchants_diff_quarter')        ,((F.col('g.sales_per_redemption_transaction_other_merchants_m0_2')-F.col('h.sales_per_redemption_transaction_other_merchants_m3_5'))/F.col('h.sales_per_redemption_transaction_other_merchants_m3_5')).alias('sales_per_redemption_transaction_other_merchants_diff_percent_quarter')        ,F.col('b.number_of_redemption_branches_other_merchants_m0').alias('number_of_redemption_branches_other_merchants_m1')        ,F.col('c.number_of_redemption_branches_other_merchants_m0').alias('number_of_redemption_branches_other_merchants_m2')        ,F.col('d.number_of_redemption_branches_other_merchants_m0').alias('number_of_redemption_branches_other_merchants_m3')        ,F.col('e.number_of_redemption_branches_other_merchants_m0').alias('number_of_redemption_branches_other_merchants_m4')        ,F.col('f.number_of_redemption_branches_other_merchants_m0').alias('number_of_redemption_branches_other_merchants_m5')        ,F.col('g.number_of_redemption_branches_other_merchants_m0_2')        ,F.col('h.number_of_redemption_branches_other_merchants_m3_5')        ,F.col('i.number_of_redemption_branches_other_merchants_m0_5')        ,(F.col('b.number_of_redemption_branches_other_merchants_m0')/F.col('aa.number_of_redemption_branches_other_merchants_m0')).alias('number_of_redemption_branches_other_merchants_ratio_m0_1')        ,(F.col('c.number_of_redemption_branches_other_merchants_m0')/F.col('aa.number_of_redemption_branches_other_merchants_m0')).alias('number_of_redemption_branches_other_merchants_ratio_m0_2')        ,(F.col('d.number_of_redemption_branches_other_merchants_m0')/F.col('aa.number_of_redemption_branches_other_merchants_m0')).alias('number_of_redemption_branches_other_merchants_ratio_m0_3')        ,(F.col('e.number_of_redemption_branches_other_merchants_m0')/F.col('aa.number_of_redemption_branches_other_merchants_m0')).alias('number_of_redemption_branches_other_merchants_ratio_m0_4')        ,(F.col('f.number_of_redemption_branches_other_merchants_m0')/F.col('aa.number_of_redemption_branches_other_merchants_m0')).alias('number_of_redemption_branches_other_merchants_ratio_m0_5')        ,(F.col('aa.number_of_redemption_branches_other_merchants_m0')-F.when(F.col('b.number_of_redemption_branches_other_merchants_m0').isNull(),0).otherwise(F.col('b.number_of_redemption_branches_other_merchants_m0'))).alias('number_of_redemption_branches_other_merchants_diff_m0_1')        ,((F.col('aa.number_of_redemption_branches_other_merchants_m0')-F.col('b.number_of_redemption_branches_other_merchants_m0'))/F.col('b.number_of_redemption_branches_other_merchants_m0')).alias('number_of_redemption_branches_other_merchants_diff_percent_m0_1')        ,(F.col('g.number_of_redemption_branches_other_merchants_m0_2')-F.when(F.col('h.number_of_redemption_branches_other_merchants_m3_5').isNull(),0).otherwise(F.col('h.number_of_redemption_branches_other_merchants_m3_5'))).alias('number_of_redemption_branches_other_merchants_diff_quarter')        ,((F.col('g.number_of_redemption_branches_other_merchants_m0_2')-F.col('h.number_of_redemption_branches_other_merchants_m3_5'))/F.col('h.number_of_redemption_branches_other_merchants_m3_5')).alias('number_of_redemption_branches_other_merchants_diff_percent_quarter')        ,F.col('g.number_of_months_redeemed_other_merchants_m0_2')        ,F.col('h.number_of_months_redeemed_other_merchants_m3_5')        ,F.col('i.number_of_months_redeemed_other_merchants_m0_5')
)

        


# In[314]:


logger.info("Final Join")


# In[346]:


df_final=df_final_merchant_issuance.join(df_final_merchant_redemption,["csn","order_no"]).join(df_final_other_merchants_issuance,["csn","order_no"]).join(df_final_other_merchants_redemption,["csn","order_no"])


# In[243]:


logger.info("Writing into %s.csv",file_name)


# In[ ]:


df_final.repartition(1).write.csv(file_name+".csv",header='true',mode='overwrite')


# In[245]:


#df_final_merchant_issuance.repartition(1).write.csv(file_name+"_issuance.csv",header='true',mode='overwrite')


# In[ ]:


#df_final.write.csv("s3a://link-lac-data/Data Mart/test1")


# In[ ]:


#df_final.write.format("csv").save('s3a://link-lac-data/Data Mart/'+folder_name+'_result/'+file_name,mode='overwrite')


# In[ ]:


#df_final.write.save('s3://link-lac-data/Data Mart/'+folder_name+'_result/'+file_name, format='csv')


# In[ ]:


#df_final.write.parquet('s3://link-lac-data/Data Mart/'+folder_name+'_result/'+file_name+'.parquet',mode='overwrite')

