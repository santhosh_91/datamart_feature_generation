# DataMart_feature_generation

This project is implemented by: ``Santhosh Kumar Ponnambalam`` Email:  ``santhosh@ntuclink.com.sg``.

------

This repository includes the source code for generating the features in the Feature_List.xlsx file. The features are generated for each month between the start and end dates.
The features are derived by the member's spend behaviour 1 to 6 months before the corresponding month ( including current month). For example, a member record on June 2018 will have 
features derived using the member's spend behaviour from January 2018 till June 2018.

### Source tables ###
The following public tables under ``link`` schema are needed:

* view_facttrans_plus


### Required packages ###
This project is written by [Python 2.7.14](https://www.python.org/downloads/release/python-2714/). The following python packages are required:

* [pyspark v2.2.0](http://spark.apache.org/docs/2.2.0/api/python/index.html)
* [SQLAlchemy v1.1.13](http://www.sqlalchemy.org)

### How to use ###

The syntax to run the program is as follows -

`python DataMart_production.py <corporation_id> <start_month> <start_year> <end_month> <end_year> <schema> <table_name> <file_name> <folder_name>`

* corporation_id: corporation of the merchant from link.link_cls_corporation table
* start_month	: starting month from when the features are required
* start_year	: starting year from when the features are required
* end_month		: end month till when the features are required
* end_year		: end year till when the features are required
* schema		: schema in which the transactions are stored in a table
* table_name	: table in which the transactions are stored

sample: 

`python DataMart_production.py 104940000000 11 2017 4 2018 link view_facttrans_plus Caltex Caltex`

This command will create the output as a csv file in the same folder.
